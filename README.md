## School Management Program

This is basically a small school management program,
which helps the end user do the following things:

1. Create **schools/universities**.
2. Create **students** for the educational facilities.
3. **Greet** the newly created students.
4. **Calculate the average grade** of **school/university**.
5. Print the **top performing student** among all facilities.
6. Print the **top performing students by gender**.
7. Calculate **the total income** for the schools/universities.
8. Print the **top contributor**.

---