package com.isoft.internship.schoolmanagement.entity;

import java.util.List;

public class School extends BaseEducationalEntity {
	
	private static final Integer SCHOOL_TAX_IN_EURO = 50;

	public School(String id, String name, String address, List<Student> students) {
		super(id, name, address, students);
	}

	@Override
	Integer getEducationalEntityTax() {
		return SCHOOL_TAX_IN_EURO;
	}
	
}
