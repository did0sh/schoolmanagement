package com.isoft.internship.schoolmanagement.entity;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import com.isoft.internship.schoolmanagement.constants.GlobalConstants;
import com.isoft.internship.schoolmanagement.enums.Gender;

public class Student {
	private static Random rand = new Random();

	private String name;
	private Integer age;
	private Gender gender;
	private Double averageGrade;
	private BaseEducationalEntity educationalEntity;
	private String entityId;

	public Student(String name, Gender gender, BaseEducationalEntity educationalEntity) {
		this.setName(name);
		this.setGender(gender);
		this.setEducationalEntity(educationalEntity);
		this.setAge();
		this.setAverageGrade();
		this.setEntityId();

		System.out.println(this.toString());
	}

	public String getName() {
		return name;
	}

	private void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	//setting a randomAge between 12 & 60
	private void setAge() {
		Integer randomAge = ThreadLocalRandom.current().nextInt(GlobalConstants.MIN_AGE_STUDENT,
				GlobalConstants.MAX_AGE_STUDENT + 1);
		this.age = randomAge;

	}

	public Gender getGender() {
		return gender;
	}

	private void setGender(Gender gender) {
		this.gender = gender;
	}

	public Double getAverageGrade() {
		return averageGrade;
	}

	//setting a random average grade between 2.00 & 6.00
	private void setAverageGrade() {
		double randomValue = GlobalConstants.MIN_GRADE
				+ (GlobalConstants.MAX_GRADE - GlobalConstants.MIN_GRADE) * rand.nextDouble();
		this.averageGrade = Math.floor(randomValue * 100) / 100;
	}

	public BaseEducationalEntity getEducationalEntity() {
		return educationalEntity;
	}

	private void setEducationalEntity(BaseEducationalEntity educationalEntity) {
		this.educationalEntity = educationalEntity;
	}

	public String getEntityId() {
		return this.entityId;
	}

	private void setEntityId() {
		this.entityId = this.getEducationalEntity().getId();
	}
	
	public Double calculatePayment() {
		return ((this.getAge() / educationalEntity.calcAverageGradeInEntity()) * 100) + educationalEntity.getEducationalEntityTax();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(
				String.format(GlobalConstants.WELCOME_MESSAGE, this.getName(), this.getEducationalEntity().getName()));
		return builder.toString();
	}

}
