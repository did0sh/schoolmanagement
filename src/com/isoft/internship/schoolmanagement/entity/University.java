package com.isoft.internship.schoolmanagement.entity;

import java.util.List;

public class University extends BaseEducationalEntity {
	
	private static final Integer UNI_TAX_IN_EURO = 100;

	public University(String id, String name, String address, List<Student> students) {
		super(id, name, address, students);
	}

	@Override
	Integer getEducationalEntityTax() {
		return UNI_TAX_IN_EURO;
	}

}
