package com.isoft.internship.schoolmanagement.entity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class BaseEducationalEntity {
	private String id;
	private String name;
	private String address;
	private List<Student> students;

	protected BaseEducationalEntity(String id, String name, String address, List<Student> students) {
		this.setId(id);
		this.setName(name);
		this.setAddress(address);
		this.setStudents(students);
	}

	public String getId() {
		return id;
	}

	private void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	private void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	private void setAddress(String address) {
		this.address = address;
	}

	public List<Student> getStudents() {
		return Collections.unmodifiableList(this.students);
	}

	private void setStudents(List<Student> students) {
		this.students = students;
	}

	public void addStudent(Student student) {
		this.students.add(student);
	}

	public Double calcAverageGradeInEntity() {
		double totalGradeEducationalEntity = this.getStudents().stream().mapToDouble(Student::getAverageGrade).sum();
		return totalGradeEducationalEntity / this.getStudents().size();

	}

	public Double calculateTotalIncome() {
		return this.getStudents().stream().mapToDouble(student -> student.calculatePayment()).sum();
	}

	public Student getTopContributor() {
		Optional<Student> topContributor = this.getStudents().stream()
				.max(Comparator.comparing(Student::calculatePayment));
		if (topContributor.isPresent()) {
			return topContributor.get();
		}
		return null;
	}

	abstract Integer getEducationalEntityTax();
}
