package com.isoft.internship.schoolmanagement.enums;

public enum EntityType {
	SCHOOL, UNIVERSITY
}
