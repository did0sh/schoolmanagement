package com.isoft.internship.schoolmanagement.factory;

import java.util.List;

import com.isoft.internship.schoolmanagement.entity.BaseEducationalEntity;
import com.isoft.internship.schoolmanagement.enums.EntityType;

public interface EducationalEntityFactory {
	public List<BaseEducationalEntity> createEducationalEntity(EntityType entityType, Integer entityCount);
}
