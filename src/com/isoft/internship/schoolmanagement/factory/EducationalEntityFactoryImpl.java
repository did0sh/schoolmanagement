package com.isoft.internship.schoolmanagement.factory;

import java.util.ArrayList;
import java.util.List;

import com.isoft.internship.schoolmanagement.constants.GlobalConstants;
import com.isoft.internship.schoolmanagement.entity.BaseEducationalEntity;
import com.isoft.internship.schoolmanagement.entity.School;
import com.isoft.internship.schoolmanagement.entity.Student;
import com.isoft.internship.schoolmanagement.entity.University;
import com.isoft.internship.schoolmanagement.enums.EntityType;

public class EducationalEntityFactoryImpl implements EducationalEntityFactory {

	@Override
	public List<BaseEducationalEntity> createEducationalEntity(EntityType entityType, Integer entityCount) {
	
		Integer numberAppender = 0;
		List<BaseEducationalEntity> educationalEntities = new ArrayList<>();
		
		switch(entityType) {
		case SCHOOL:
			for (int i = 0; i < entityCount; i++) {
				numberAppender++;
				educationalEntities.add(this.createSchool(numberAppender));
			}
			return educationalEntities;
		case UNIVERSITY:
			for (int i = 0; i < entityCount; i++) {
				numberAppender++;
				educationalEntities.add(this.createUniversity(numberAppender));
			}
			return educationalEntities;
			default:
				return new ArrayList<>();
		}
	}
	
	private University createUniversity(Integer numberAppender) {
		return new University(GlobalConstants.UNIVERSITY_ID.concat(String.valueOf(numberAppender)), 
				GlobalConstants.UNIVERSITY_NAME.concat(String.valueOf(numberAppender)), 
				GlobalConstants.UNIVERSITY_SCHOOL_ADDRESS.concat(String.valueOf(numberAppender)), 
						new ArrayList<Student>());
	}
	
	private School createSchool(Integer numberAppender) {
		return new School(GlobalConstants.SCHOOL_ID.concat(String.valueOf(numberAppender)), 
				GlobalConstants.SCHOOL_NAME.concat(String.valueOf(numberAppender)), 
				GlobalConstants.UNIVERSITY_SCHOOL_ADDRESS.concat(String.valueOf(numberAppender)), 
						new ArrayList<Student>());
	}
	
}
