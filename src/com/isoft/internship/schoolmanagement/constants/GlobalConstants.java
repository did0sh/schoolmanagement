package com.isoft.internship.schoolmanagement.constants;

public class GlobalConstants {
	public static final String WELCOME_MESSAGE = "Hello %s and welcome to %s.";
	
	public static final String UNIVERSITY_ID = "UN";
	public static final String UNIVERSITY_NAME = "University";

	public static final String SCHOOL_ID = "SCH";
	public static final String SCHOOL_NAME = "School";

	public static final String UNIVERSITY_SCHOOL_ADDRESS = "Address";

	public static final String SCHOOL_STUDENT_NAME = "SchoolStudent";
	public static final String UNI_STUDENT_NAME = "UniStudent";

	public static final Integer COUNT_OF_SCHOOLS = 3;
	public static final Integer COUNT_OF_UNIVERSITIES = 2;
	public static final Integer NUMBER_OF_STUDENTS_IN_A_SCHOOL = 10;
	public static final Integer NUMBER_OF_STUDENTS_IN_A_UNIVERSITY = 10;
	
	public static final Double MIN_GRADE = 2.00;
	public static final Double MAX_GRADE = 6.00;
	
	public static final String AVERAGE_GRADE_PER_ENTITY_FORMAT = "%s - (%.2f)%n";
	public static final Integer MIN_AGE_STUDENT = 12;
	public static final Integer MAX_AGE_STUDENT = 60;
	
	public static final String TOP_PERFORMING_STUDENT_FORMAT = "%s from %s - (%.2f)%n";
	public static final String TOP_PERFORMING_STUDENT_BY_GENDER_FORMAT = "%s from %s - %s (%.2f)%n";
	
	public static final String TOP_CONTRIBUTOR_STUDENT_FORMAT = "%s - Facility: %s, Age: %s, Gender: %s, Payment: %.2f%n";
}
