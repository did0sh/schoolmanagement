package com.isoft.internship.schoolmanagement;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.isoft.internship.schoolmanagement.constants.GlobalConstants;
import com.isoft.internship.schoolmanagement.entity.BaseEducationalEntity;
import com.isoft.internship.schoolmanagement.entity.Student;
import com.isoft.internship.schoolmanagement.enums.EntityType;
import com.isoft.internship.schoolmanagement.enums.Gender;
import com.isoft.internship.schoolmanagement.factory.EducationalEntityFactory;
import com.isoft.internship.schoolmanagement.factory.EducationalEntityFactoryImpl;

public class Application {
	private static final Logger LOGGER = Logger.getLogger(Application.class.getName());

	public static void main(String[] args) {
		EducationalEntityFactory educationalFactory = new EducationalEntityFactoryImpl();

		List<BaseEducationalEntity> schools = educationalFactory.createEducationalEntity(EntityType.SCHOOL,
				GlobalConstants.COUNT_OF_SCHOOLS);
		List<BaseEducationalEntity> universities = educationalFactory.createEducationalEntity(EntityType.UNIVERSITY,
				GlobalConstants.COUNT_OF_UNIVERSITIES);

		List<BaseEducationalEntity> allFacilities = new ArrayList<>(schools);
		allFacilities.addAll(universities);

		createStudentsForSchool(GlobalConstants.NUMBER_OF_STUDENTS_IN_A_SCHOOL, schools);
		createStudentsForUniversity(GlobalConstants.NUMBER_OF_STUDENTS_IN_A_UNIVERSITY, universities);

		printAverageGradeOfStudentsPerFacility(allFacilities);
		findTopPerformingStudent(allFacilities);
		findTopPerformingStudentByGender(allFacilities);
		printTopContributor(allFacilities);

	}

	private static void createStudentsForSchool(Integer studentsCount, List<BaseEducationalEntity> schools) {
		LOGGER.log(Level.INFO, "Schools: ------------------------------");
		Student student = null;
		int count = 1;
		for (BaseEducationalEntity baseEducationalEntity : schools) {
			for (int i = count; i <= studentsCount; i++) {

				// males and females distributed 50%
				if (i <= studentsCount / 2) {
					student = new Student(GlobalConstants.SCHOOL_STUDENT_NAME.concat(String.valueOf(i)), Gender.MALE,
							baseEducationalEntity);
				} else {
					student = new Student(GlobalConstants.SCHOOL_STUDENT_NAME.concat(String.valueOf(i)), Gender.FEMALE,
							baseEducationalEntity);
				}
				baseEducationalEntity.addStudent(student);
			}
			System.out.println();
		}

	}

	private static void createStudentsForUniversity(Integer studentsCount, List<BaseEducationalEntity> universities) {
		LOGGER.log(Level.INFO, "Universities: ------------------------------");
		Student student = null;
		int count = 1;
		for (BaseEducationalEntity baseEducationalEntity : universities) {
			for (int i = count; i <= studentsCount; i++) {

				// males and females distributed 50%
				if (i <= studentsCount / 2) {
					student = new Student(GlobalConstants.UNI_STUDENT_NAME.concat(String.valueOf(i)), Gender.MALE,
							baseEducationalEntity);
				} else {
					student = new Student(GlobalConstants.UNI_STUDENT_NAME.concat(String.valueOf(i)), Gender.FEMALE,
							baseEducationalEntity);
				}
				baseEducationalEntity.addStudent(student);
			}
			System.out.println();
		}
	}

	private static void printAverageGradeOfStudentsPerFacility(List<BaseEducationalEntity> allFacilities) {
		LOGGER.log(Level.INFO, "Average Grade Per School/University: ------------------------------");
		allFacilities.stream().forEach(entity -> System.out.printf(GlobalConstants.AVERAGE_GRADE_PER_ENTITY_FORMAT,
				entity.getName(), entity.calcAverageGradeInEntity()));
	}

	private static void findTopPerformingStudent(List<BaseEducationalEntity> allFacilities) {
		LOGGER.log(Level.INFO, "Top Performing Student: ------------------------------");
		List<Student> allStudents = new ArrayList<>();
		for (BaseEducationalEntity baseEducationalEntity : allFacilities) {
			Optional<Student> topPerformingSchoolStudent = baseEducationalEntity.getStudents().stream()
					.max(Comparator.comparing(Student::getAverageGrade));

			if (topPerformingSchoolStudent.isPresent()) {
				Student topPerfomerFromCurrentFacility = topPerformingSchoolStudent.get();
				allStudents.add(topPerfomerFromCurrentFacility);
			}

		}

		Optional<Student> topPerformer = allStudents.stream().max(Comparator.comparing(Student::getAverageGrade));
		if (topPerformer.isPresent()) {
			Student topPerformingStudent = topPerformer.get();
			System.out.printf(GlobalConstants.TOP_PERFORMING_STUDENT_FORMAT, topPerformingStudent.getName(),
					topPerformingStudent.getEducationalEntity().getName(), topPerformingStudent.getAverageGrade());
		}
	}

	private static void findTopPerformingStudentByGender(List<BaseEducationalEntity> allFacilities) {
		LOGGER.log(Level.INFO, "Top Performing Students By Gender: ------------------------------");
		List<Student> allStudentsByGender = new ArrayList<>();

		for (BaseEducationalEntity baseEducationalEntity : allFacilities) {

			Optional<Student> topPerformingSchoolStudentMALE = baseEducationalEntity.getStudents().stream()
					.filter(student -> student.getGender() == Gender.MALE)
					.max(Comparator.comparing(Student::getAverageGrade));

			Optional<Student> topPerformingSchoolStudentFEMALE = baseEducationalEntity.getStudents().stream()
					.filter(student -> student.getGender() == Gender.FEMALE)
					.max(Comparator.comparing(Student::getAverageGrade));

			if (topPerformingSchoolStudentMALE.isPresent()) {
				allStudentsByGender.add(topPerformingSchoolStudentMALE.get());
			}

			if (topPerformingSchoolStudentFEMALE.isPresent()) {
				allStudentsByGender.add(topPerformingSchoolStudentFEMALE.get());
			}
		}

		Optional<Student> topPerformerMALE = allStudentsByGender.stream()
				.filter(student -> student.getGender() == Gender.MALE)
				.max(Comparator.comparing(Student::getAverageGrade));
		Optional<Student> topPerformerFEMALE = allStudentsByGender.stream()
				.filter(student -> student.getGender() == Gender.FEMALE)
				.max(Comparator.comparing(Student::getAverageGrade));

		if (topPerformerMALE.isPresent()) {
			allStudentsByGender.add(topPerformerMALE.get());
			System.out.printf(GlobalConstants.TOP_PERFORMING_STUDENT_BY_GENDER_FORMAT, topPerformerMALE.get().getName(),
					topPerformerMALE.get().getEducationalEntity().getName(), topPerformerMALE.get().getGender(),
					topPerformerMALE.get().getAverageGrade());
		}

		if (topPerformerFEMALE.isPresent()) {
			allStudentsByGender.add(topPerformerFEMALE.get());
			System.out.printf(GlobalConstants.TOP_PERFORMING_STUDENT_BY_GENDER_FORMAT,
					topPerformerFEMALE.get().getName(), topPerformerFEMALE.get().getEducationalEntity().getName(),
					topPerformerFEMALE.get().getGender(), topPerformerFEMALE.get().getAverageGrade());
		}
	}

	private static void printTopContributor(List<BaseEducationalEntity> allFacilities) {
		LOGGER.log(Level.INFO, "Top Contributor: ------------------------------");
		List<Student> topContributors = new ArrayList<>();
		allFacilities.forEach(entity -> topContributors.add(entity.getTopContributor()));

		Optional<Student> topContributor = topContributors.stream()
				.max(Comparator.comparing(Student::calculatePayment));
		
		if(topContributor.isPresent()) {
			System.out.printf(GlobalConstants.TOP_CONTRIBUTOR_STUDENT_FORMAT, topContributor.get().getName(),
					topContributor.get().getEducationalEntity().getName(), topContributor.get().getAge(), topContributor.get().getGender(),
					topContributor.get().calculatePayment());
		}
	}

}
